package core.mobile;

import core.ArticleHelper;
import core.BaseFunctions;
import core.CommentHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wrappers.ArticleWrapper;

import java.util.ArrayList;
import java.util.List;

public class HomePageMob {

    private BaseFunctions baseFunc;
    private ArticleHelper ah = new ArticleHelper();
    private CommentHelper ch = new CommentHelper();
    private static final Logger LOGGER = LogManager.getLogger(HomePageMob.class);
    private static final By ARTICLE = By.cssSelector("[class='md-mosaic-title']");
    private static final By TITLE = By.xpath("//a[@class='md-scrollpos']");
    private static final By COMMENT_COUNT = By.xpath("//a[@class='commentCount']");

    public HomePageMob(BaseFunctions bs) {
        LOGGER.info("Home page with mobile version is loaded");
        this.baseFunc = bs;
    }

    public List<String> linksToArticles(int linksAmount) {
        List<String> listOfLinksToArticles = new ArrayList<>();
        for (int i = 0; i < linksAmount; i++) {
            List<WebElement> listOfAllArticles = baseFunc.getWebElementsList(ARTICLE);
            WebElement elements = listOfAllArticles.get(i);
            String link = elements.findElements(TITLE).get(i).getAttribute("href");
            listOfLinksToArticles.add(link);
        }
        return listOfLinksToArticles;
    }

    public List<String> getTitles(int titleAmount) {
        List<String> titleList = new ArrayList<>();
        List<ArticleWrapper> articleList = ah.getListArticles(ARTICLE, baseFunc);
        for (int i = 0; i < titleAmount; i++) {
            String title = articleList.get(i).getTitle(TITLE, i);
            titleList.add(title);
            LOGGER.info("Title number: " + i + " '' " + titleList.get(i) + " '' " + "added to the list");
        }
        return titleList;
    }

    public List<Integer> getCommentsCount(int commentAmount) {
        List<Integer> listCommentsCount = new ArrayList<>();
        List<ArticleWrapper> articleList = ah.getListArticles(ARTICLE, baseFunc);
        for (int i = 0; i < commentAmount; i++) {
            String comments = articleList.get(i).getCommentCount(COMMENT_COUNT,i);
            System.out.println(comments);
            int commentCounts = ch.stringToInt(comments);
            listCommentsCount.add(commentCounts);
            LOGGER.info("Comment count: " + i + " '' " + listCommentsCount.get(i) + " '' " + "added to the list");
        }
        return listCommentsCount;
    }

    public void clickComment(Integer iteration) {
        LOGGER.info("Clicking comment");
        baseFunc.clickElements(COMMENT_COUNT, iteration);
    }
}
