package core.mobile;

import core.BaseFunctions;
import core.CommentHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CommentPageMob {

    private CommentHelper ch = new CommentHelper();
    private BaseFunctions baseFunc;
    private static final By TITLE = By.xpath("//span[@class='text']");
    private static final By COMMENT_COUNT_REGISTERED = By.xpath("//a[contains(text(), 'Reģistrētie')]");
    private static final By COMMENT_COUNT_ANONYMOUS = By.xpath("//a[contains(text(), 'Anonīmie')]");

    public CommentPageMob(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public String getTitle() {
        WebElement element = baseFunc.getElement(TITLE);
        return element.getText();
    }

    public Integer getRegisteredCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT_REGISTERED);
        return ch.stringToInt(element.getText());
    }

    public Integer getAnonymousCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT_ANONYMOUS);
        return ch.stringToInt(element.getText());
    }
}
