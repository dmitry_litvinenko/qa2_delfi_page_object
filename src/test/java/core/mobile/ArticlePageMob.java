package core.mobile;

import core.BaseFunctions;
import core.CommentHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticlePageMob {

    private CommentHelper ch = new CommentHelper();
    private BaseFunctions baseFunc;
    private static final By TITLE = By.xpath("//*[@id='article']/div/h1");
    private static final By COMMENT_COUNT = By.xpath("//div/a[@class='commentCount']");

    public ArticlePageMob(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public String getTitle() {
        WebElement element = baseFunc.getElement(TITLE);
        return element.getText();
    }

    public Integer getCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT);
        return ch.stringToInt(element.getText());
    }

}
