package core.desktop;

import core.BaseFunctions;
import core.CommentHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class CommentPageDesk {

    private CommentHelper ch = new CommentHelper();
    private BaseFunctions baseFunc;
    private static final By TITLE = By.xpath("//a[@class='comment-main-title-link']");
    private static final By COMMENT_COUNT_REGISTERED = By.xpath("//*[contains(@class,'-list-a-reg')]");
    private static final By COMMENT_COUNT_ANONYMOUS = By.xpath("//*[contains(@class,'list-a-anon')]");

    public CommentPageDesk(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public String getTitle() {
        return baseFunc.getElement(TITLE).getText();
    }

    public Integer getRegisteredCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT_REGISTERED);
        return ch.stringToInt(element.getText());
    }

    public Integer getAnonymousCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT_ANONYMOUS);
        return ch.stringToInt(element.getText());
    }
}
