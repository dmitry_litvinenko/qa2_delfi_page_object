package core.desktop;

import core.BaseFunctions;
import core.CommentHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ArticlePageDesk {

    private CommentHelper ch = new CommentHelper();
    private BaseFunctions baseFunc;
    private static final By TITLE = By.xpath("//span[@itemprop='headline name']");
    private static final By COMMENT_COUNT = By.xpath("//div/a[@class='comment-count']");

    public ArticlePageDesk(BaseFunctions bs) {
        this.baseFunc = bs;
    }

    public String getTitle() {
        return baseFunc.getElement(TITLE).getText();
    }

    public Integer getCommentCount() {
        WebElement element = baseFunc.getElement(COMMENT_COUNT);
        return ch.stringToInt(element.getText());
    }


}
