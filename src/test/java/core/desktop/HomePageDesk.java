package core.desktop;

import core.ArticleHelper;
import core.BaseFunctions;
import core.CommentHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import tests.TestArticlesDesktop;
import wrappers.ArticleWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class HomePageDesk {

    private BaseFunctions baseFunc;
    private CommentHelper ch = new CommentHelper();
    private ArticleHelper ah = new ArticleHelper();
    private static final By ARTICLE = By.xpath("//h3[@class='top2012-title']");
    private static final By TITLE = By.xpath("//h3/a[@class='top2012-title']");
    private static final By COMMENT_COUNT = By.xpath("//h3/a[@class='comment-count']");
    private static final Logger LOGGER = LogManager.getLogger(TestArticlesDesktop.class);

    public HomePageDesk(BaseFunctions bs) {
        LOGGER.info("Home page is loaded");
        this.baseFunc = bs;
    }

    public List<String> linksToArticles(int linksAmount) {
        List<String> listOfLinksToArticles = new ArrayList<>();
        List<WebElement> listOfAllArticles = baseFunc.getWebElementsList(ARTICLE);
        for (int i = 0; i < linksAmount; i++) {
            listOfLinksToArticles.add(listOfAllArticles.get(i).findElements(TITLE).get(i).getAttribute("href"));
        }
        return listOfLinksToArticles;
    }

    public void clickComment(Integer iteration) {
        LOGGER.info("Clicking comment");
        baseFunc.clickElements(COMMENT_COUNT, iteration);
    }

    public List<String> getTitles(int titleAmount) {
        List<String> titleList = new ArrayList<>();
        List<ArticleWrapper> articleList = ah.getListArticles(ARTICLE, baseFunc);
        for (int i = 0; i < titleAmount; i++) {
            String title = articleList.get(i).getTitle(TITLE, i);
            titleList.add(title);
            LOGGER.info("Title number: " + i + " '' " + titleList.get(i) + " '' " + "added to the list");
        }
        return titleList;

    }

    public List<Integer> getCommentsCount(int commentAmount) {
        List<Integer> listCommentsCount = new ArrayList<>();
        List<ArticleWrapper> articleList = ah.getListArticles(ARTICLE, baseFunc);
        for (int i = 0; i < commentAmount; i++) {
            String comments = articleList.get(i).getCommentCount(COMMENT_COUNT, i);
            int commentCounts = ch.stringToInt(comments);
            listCommentsCount.add(commentCounts);
            LOGGER.info("Comment count: " + i + " '' " + listCommentsCount.get(i) + " '' " + "added to the list");
        }
        return listCommentsCount;
    }

}
