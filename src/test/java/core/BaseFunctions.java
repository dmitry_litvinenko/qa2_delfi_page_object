package core;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseFunctions {

    private WebDriver driver;
    private static final String CHROME_DRIVER_LOCATION = "D://Workspace/chromedriver.exe";
    private static final Logger LOGGER = LogManager.getLogger(BaseFunctions.class);

    public BaseFunctions() {
        LOGGER.info("Setting chromedriver for desktop version");
        LOGGER.info("Chromedriver location: " + CHROME_DRIVER_LOCATION);
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_LOCATION);

        LOGGER.info("Starting Chrome Driver");
        this.driver = new ChromeDriver();
    }

    public BaseFunctions(String mobileDevice) {

        LOGGER.info("Setting chromedriver for mobile version");
        LOGGER.info("Chromedriver location: " + CHROME_DRIVER_LOCATION);
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_LOCATION);

        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", mobileDevice);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        this.driver = new ChromeDriver(chromeOptions);

    }

    /**
     * This method goes to URL
     *
     * @param url web address
     */
    public void openWebPage(String url) {
        if (!url.contains("http://") && !url.contains("https://")) {
            url = "http://" + url;
        }
        LOGGER.info("Opening web page: " + url + " \n");
        driver.get(url);
    }

    /**
     * This method getting web element list
     *
     * @param element locator for the web element
     */
    public List<WebElement> getWebElementsList(By element) {
        return driver.findElements(element);
    }

    /**
     * Method to click on the specific element
     *
     * @param locator     element to click
     * @param indexNumber element index number.
     */
    public void clickElements(By locator, int indexNumber) {
        WebElement element = driver.findElements(locator).get(indexNumber);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    /**
     * Method getting web element
     *
     * @param element web element
     */
    public WebElement getElement(By element) {
        return driver.findElement(element);
    }

    /**
     * Method returns a list of elements with a specific locator
     *
     * @param element element locator to search
     * @return list of WebElements
     */
    public List<WebElement> getElements(By element) {
        return driver.findElements(element);
    }

    /**
     * Method to close the web driver
     */
    public void closeDriver() {
        LOGGER.info("Stopping driver");
        driver.close();
    }

}
