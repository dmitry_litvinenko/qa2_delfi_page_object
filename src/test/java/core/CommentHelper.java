package core;


import com.google.common.base.CharMatcher;

public class CommentHelper {

    /**
     * This method is returning number from the text
     * Zero's are not included
     *
     * @param textInput text that will be returned as an integer values
     */
    public Integer stringToInt(String textInput) {
        String digits = CharMatcher.digit().retainFrom(textInput);
        return Integer.valueOf(digits);
    }
}
