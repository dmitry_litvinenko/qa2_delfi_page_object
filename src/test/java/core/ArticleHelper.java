package core;

import com.google.common.collect.Iterables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wrappers.ArticleWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ArticleHelper {

    public List<ArticleWrapper> getListArticles(By article, BaseFunctions baseFunc) {
        List<WebElement> articles = baseFunc.getElements(article);
        List<ArticleWrapper> articleWrappers = new ArrayList<>();
        Iterables.addAll(articleWrappers,
                articles.stream()
                        .map(webElement -> new ArticleWrapper(baseFunc, webElement))
                        .collect(Collectors.toList()));
        return articleWrappers;
    }
}

