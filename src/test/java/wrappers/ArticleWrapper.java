package wrappers;

import core.BaseFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticleWrapper {

    private final BaseFunctions baseFunc;
    private final WebElement element;

    public ArticleWrapper(BaseFunctions bs, WebElement element) {
        this.baseFunc = bs;
        this.element = element;
    }

    public String getTitle(By title, int titleNumber) {
        return !element.findElements(title).isEmpty() ? element.findElements(title).get(titleNumber).getText() : null;
    }

    public String getCommentCount(By commentCount, int titleNumber) {
        return !element.findElements(commentCount).isEmpty() ? element.findElements(commentCount).get(titleNumber).getText() : null;
    }

}

