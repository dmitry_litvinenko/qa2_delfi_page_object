package tests;

import core.BaseFunctions;
import core.mobile.ArticlePageMob;
import core.mobile.CommentPageMob;
import core.mobile.HomePageMob;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class TestArticlesMob {

    private BaseFunctions baseFunc = new BaseFunctions("Nexus 5");
    private HomePageMob hp = new HomePageMob(baseFunc);
    private ArticlePageMob ap = new ArticlePageMob(baseFunc);
    private CommentPageMob cp = new CommentPageMob(baseFunc);
    private static final Logger LOGGER = LogManager.getLogger(TestArticlesMob.class);
    private static final String HOME_PAGE = "delfi.lv";
    private static final int ITERATIONS_AMOUNT = 5;

    private List<String> listLinksToArticles;
    private List<String> listTitles;
    private List<Integer> listCommentsCount;

    @BeforeTest
    public void beforeTest() {
        LOGGER.info("STARTING BEFORE TEST");
        baseFunc.openWebPage(HOME_PAGE);

        LOGGER.info("STARTING MOBILE ARTICLES SET UP");

        LOGGER.info("GETTING ALL LINK TO ARTICLES");
        listLinksToArticles = hp.linksToArticles(ITERATIONS_AMOUNT);
        System.out.println(listLinksToArticles);

        LOGGER.info("GETTING ARTICLES TITLES FROM THE HOME PAGE");
        listTitles = hp.getTitles(ITERATIONS_AMOUNT);

        LOGGER.info("GETTING ARTICLES TITLES COMMENTS COUNT FROM THE HOME PAGE");
        listCommentsCount = hp.getCommentsCount(ITERATIONS_AMOUNT);

        LOGGER.info("ARTICLE SET UP COMPLETED");
        LOGGER.info("BEFORE TEST COMPLETED");

    }

    @DataProvider(name = "links to articles")
    public Object[][] dataProvider() {
        Object[][] data = new Object[ITERATIONS_AMOUNT][2];

        for (int i = 0; i < ITERATIONS_AMOUNT; i++) {
            data[i][0] = listLinksToArticles.get(i);
            data[i][1] = i;
        }
        return data;
    }

    @Test(dataProvider = "links to articles")
    public void testArticlesPageTitles(String urlToArticle, Integer iteration) {
        LOGGER.info("STARTING TEST " + iteration + " ARTICLE PAGE FOR TITLES");

        LOGGER.info("GO TO ARTICLE PAGE");
        baseFunc.openWebPage(urlToArticle);

        LOGGER.info("GETTING TITLE FROM THE ARTICLE PAGE");
        String articlePageTitle = ap.getTitle();

        LOGGER.info("COMPARING TITLES FROM THE HOME PAGE WITH ARTICLE PAGE");
        Assert.assertEquals(articlePageTitle, listTitles.get(iteration));

        LOGGER.info("TEST NUMBER: " + iteration + " SUCCESSFUL \n");
    }

    @Test(dataProvider = "links to articles")
    public void testArticlesPageCommentCount(String urlToArticle, Integer iteration) {
        LOGGER.info("STARTING TEST " + iteration + " ARTICLE PAGE FOR COMMENT COUNT");

        LOGGER.info("GO TO ARTICLE PAGE");
        baseFunc.openWebPage(urlToArticle);

        LOGGER.info("GETTING COMMENT COUNT FROM THE ARTICLE PAGE");
        Integer articlePageTitle = ap.getCommentCount();

        LOGGER.info("COMPARING COMMENT COUNT FROM THE HOME PAGE WITH ARTICLE PAGE");
        Assert.assertEquals(articlePageTitle, listCommentsCount.get(iteration));

        LOGGER.info("TEST NUMBER: " + iteration + " SUCCESSFUL \n");
    }

    @Test(dataProvider = "links to articles")
    public void testCommentPageCommentCount(String urlToArticle, Integer iteration) {
        LOGGER.info("STARTING TEST " + iteration + " COMMENT PAGE FOR COMMENTS COUNT");

        LOGGER.info("GO TO HOME PAGE");
        baseFunc.openWebPage(HOME_PAGE);

        LOGGER.info("GO TO COMMENTS PAGE");
        hp.clickComment(iteration);

        LOGGER.info("GET REGISTERED COMMENT COUNT FROM THE COMMENT PAGE");
        Integer registeredComment = cp.getRegisteredCommentCount();

        LOGGER.info("GET ANONYMOUS COMMENT COUNT FROM THE COMMENT PAGE");
        Integer anonymousComment = cp.getAnonymousCommentCount();

        LOGGER.info("COMPARING COMMENT COUNT FROM THE HOME PAGE WITH COMMENT PAGE");
        Integer commentSum = registeredComment + anonymousComment;
        Assert.assertEquals(commentSum, listCommentsCount.get(iteration));

        LOGGER.info("TEST NUMBER: " + iteration + " SUCCESSFUL \n");
    }

    @Test(dataProvider = "links to articles")
    public void testCommentPageTitles(String urlToArticle, Integer iteration) {
        LOGGER.info("STARTING TEST " + iteration + " COMMENT PAGE FOR TITLES");

        LOGGER.info("GO TO HOME PAGE");
        baseFunc.openWebPage(HOME_PAGE);

        LOGGER.info("GO TO COMMENTS PAGE");
        hp.clickComment(iteration);

        LOGGER.info("GET TITLES FROM THE COMMENT PAGE");
        String title = cp.getTitle();

        LOGGER.info("COMPARING COMMENT COUNT FROM THE HOME PAGE WITH COMMENT PAGE");
        Assert.assertTrue(title.contains(listTitles.get(iteration)));

        LOGGER.info("TEST NUMBER: " + iteration + " SUCCESSFUL \n");
    }

    @AfterTest
    public void afterTest() {
        LOGGER.info("STARTING AFTER TEST");
        baseFunc.closeDriver();
    }


}

